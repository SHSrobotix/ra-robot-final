package state_derive;

import state_machine.ManagerStatus;
import state_machine.StateManager;

import java.util.Arrays;
import java.util.List;

import common.IDisplay;
import driver_modes.MODE_Dead;
import driver_modes.MODE_DeadCat;
import driver_modes.MODE_FullSpeedDrive;
import driver_modes.MODE_HalfSpeedDrive;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import state_derive.Button;
import state_derive.Context;

public class ModeStateManager extends StateManager<Context> implements IDisplay {

	Context context;
	
	public ModeStateManager(Context context) {
		super();
		if(context==null) {
			throw new IllegalArgumentException();
		}
		SetState(Build(), ManagerStatus.RUNNING);
		this.context = context;
	}
	
	ModeState Build() {
		ModeState halfSpeed = new ModeState(new MODE_HalfSpeedDrive());
		ModeState fullSpeed = new ModeState(new MODE_FullSpeedDrive());
		ModeState dead 		= new ModeState(new MODE_Dead());
		ModeState deadCat	= new ModeState(new MODE_DeadCat());
		
		List<Button> halfCombo = Arrays.asList(Button.X());
		List<Button> fullCombo = Arrays.asList(Button.B());
		List<Button> deadCombo = Arrays.asList(Button.X(), Button.X());
		List<Button> dCatCombo = Arrays.asList(Button.X(), Button.X(), Button.Y());
		
		halfSpeed.AddState(fullCombo, fullSpeed);
		halfSpeed.AddState(deadCombo, dead);
		
		fullSpeed.AddState(halfCombo, halfSpeed);
		fullSpeed.AddState(deadCombo, dead);
		
		dead.AddState(halfCombo, halfSpeed);
		dead.AddState(fullCombo, fullSpeed);
		dead.AddState(dCatCombo, deadCat);
		
		deadCat.AddState(deadCombo, dead);
		
		
		
		return halfSpeed;
	}

	@Override
	public void Display() {
		SmartDashboard.putString("CurrentMode", ((ModeState)current).Name());
		SmartDashboard.putBoolean("Is Listening", ((ModeState)current).IsListening());
		context.Display();
	}
	
	
}
