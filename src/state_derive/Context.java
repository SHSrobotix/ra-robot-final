package state_derive;

import common.IDisplay;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import subsystems.DriveSystem;
import subsystems.Kicker;
import wrappers.XController;

public class Context implements IDisplay{
	public Kicker kicker = new Kicker();
	public DriveSystem driveSystem = new DriveSystem();
	public XController controller = new XController(0);
	
	public void Display() {
		SmartDashboard.putBoolean("CompressorClosedLoop", kicker.GetCompressor());
		SmartDashboard.putBoolean("Solenoid Activated", kicker.GetSolenoid());
		
	}
}
