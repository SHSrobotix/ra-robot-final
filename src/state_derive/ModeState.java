package state_derive;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import state_derive.dMode;
import state_derive.Button;
import state_machine.IState;
import state_machine.StateSignal;
import state_machine.StateStatus;
import wrappers.XController;
import state_derive.Context;

public class ModeState implements IState<Context> {

	protected dMode<Context> this_mode;
	protected HashMap<List<Button>, ModeState> mode_map = new HashMap<List<Button>, ModeState>();
	protected boolean listening = false;
	
	protected List<Button> current_combo;
	
	public ModeState(dMode<Context> mode) {
		if(mode == null) {
			throw new IllegalArgumentException();
		}
		this_mode = mode;
		current_combo = new ArrayList<Button>();
	}
	
	public void AddState(List<Button> combo, ModeState state) {
		if (combo == null) {
			throw new IllegalArgumentException();
		}
		
		if (state == null) {
			throw new IllegalArgumentException();
		}
		
		mode_map.put(combo, state);
	}
	
	public String Name() {
		return this_mode.ModeName();
	}
	
	public boolean IsListening() {
		return listening;
	}
	
	@Override
	public final StateSignal<Context> Update(Context context) {
		if(context.controller.PressedA()) {
			return ToggleListening();
		} else if (context.controller.PressedBack()) {
			//Cancel Combo
			current_combo.clear();
			listening = false;
		}
		
		if (listening == false) {
			//update mode
			IState<Context> next = this_mode.Update(context);
			if (next !=null) {
				return new StateSignal<Context>(next, StateStatus.Change);
			}
		} else if (listening) {
			Button button = MatchButton(context.controller);
			if (button !=null) {
				//This one took me awhile. I MISS RUSTTTTTT
				current_combo.add(MatchButton(context.controller));
			}
		}
		
		return new StateSignal<Context>(null, StateStatus.Continue);
	}
	
	StateSignal<Context> ToggleListening() {
		if(listening) {
			//EndListening
			listening = false;
			ModeState next = mode_map.get(current_combo);
			current_combo.clear();
			
			if(next !=null) {
				System.out.println("Changing mode...");
				return new StateSignal<Context>(next, StateStatus.Change);
			}
			else{
				System.out.println("Next mode was null");
				return new StateSignal<Context>(null, StateStatus.Continue);
			}
		} else {
			//Start listening
			listening = true;
			return new StateSignal<Context>(null, StateStatus.Continue);
		}
	}
	
	static Button MatchButton(XController controller) {
		if (controller.PressedA())
			return Button.A();
		else if (controller.PressedB())
			return Button.B();
		else if(controller.PressedY())
			return Button.Y();
		else if(controller.PressedX())
			return Button.X();
		
		return null;
	}

	@Override
	public void EnterState(Context context) {
		this_mode.EnterMode(context);
	}

	@Override
	public void ExitState(Context context) {
		this_mode.EnterMode(context);
		
	}
}
