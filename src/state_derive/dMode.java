package state_derive;

import java.util.List;

import state_derive.Button;
import state_machine.IState;

public abstract class dMode<InputData> {
	public abstract String ModeName();
	public abstract List<Button> DefaultCombo();
	
	public abstract void EnterMode(InputData context);
	public abstract IState<InputData> Update(InputData context);
	public abstract void ExitMode(InputData context);
}
