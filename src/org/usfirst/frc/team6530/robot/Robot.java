package org.usfirst.frc.team6530.robot;

import edu.wpi.first.wpilibj.SampleRobot;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import state_derive.Context;
import state_derive.ModeStateManager;

public class Robot extends SampleRobot {
	
	Context context;
	ModeStateManager manager;
	
	final String defaultAuto = "Forward";
	final String customAuto = "SideToSide";
	Autonomoose auto;
	SendableChooser<String> chooser = new SendableChooser<>();
	
	@Override
	public void robotInit() {
		chooser.addDefault("Default Auto", defaultAuto);
		chooser.addObject("My Auto", customAuto);
		SmartDashboard.putData("Auto Selector", chooser);
		
		context = new Context();
		context.kicker.Retract();
		manager = new ModeStateManager(context);
		auto = new Autonomoose(context);
	}

	@Override
	public void autonomous() {
		auto.Update();
	}

	@Override
	public void operatorControl() {
		context.driveSystem.setSafetyEnabled(true);
		while (isOperatorControl() && isEnabled()) {
			context.controller.Poll();
			manager.Update(context);
			manager.Display();
		}
	}

	@Override
	protected void disabled() {
		auto.Reset();
	}
	
	@Override
	public void test() {
	}
}
