package org.usfirst.frc.team6530.robot;

import edu.wpi.first.wpilibj.command.Scheduler;
import state_derive.Context;

public class Autonomoose {

	Context context;
	
	//dt = 0.01 seconds = 10 ms (pulled from SampleRobot main loop)
	public Autonomoose(Context context) {
		context.kicker.SetCompressor(false);
		this.context = context;
	}
	
	public void Update() {
		Scheduler.getInstance().run();
	}
	
	public void Reset() {
		
	}
}
