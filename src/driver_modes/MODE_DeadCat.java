package driver_modes;

import java.util.Arrays;
import java.util.List;

import state_derive.Button;
import state_derive.Context;
import state_derive.dMode;
import state_machine.IState;

public class MODE_DeadCat extends dMode<Context> {

	@Override
	public List<Button> DefaultCombo() {
		// TODO Auto-generated method stub
		return Arrays.asList(Button.X(), Button.X(), Button.Y());
	}

	@Override
	public void EnterMode(Context context) {
		context.driveSystem.SetDrive(0, 0);
		// TODO Auto-generated method stub
		
	}

	@Override
	public IState<Context> Update(Context context) {
		context.kicker.Kick();
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void ExitMode(Context context) {
		context.kicker.Retract();
	}

	@Override
	public String ModeName() {
		// TODO Auto-generated method stub
		return "Dead Cat";
	}

}
