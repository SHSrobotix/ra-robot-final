package driver_modes;

import java.util.Arrays;
import java.util.List;

import state_derive.Button;
import state_derive.Context;

public class MODE_HalfSpeedDrive  extends MODE_LinearDrive {

	static double m = 0.635;
	static double b = 0.0;
	
	public MODE_HalfSpeedDrive() {
		super(m, b);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public List<Button> DefaultCombo() {
		return Arrays.asList(Button.X());
	}

	@Override
	public void EnterMode(Context context) {
		// Do nothing
		
	}

	@Override
	public void ExitMode(Context context) {
		// Do nothing
		
	}

	@Override
	public String ModeName() {
		// TODO Auto-generated method stub
		return "Half Speed Drive";
	}
}
