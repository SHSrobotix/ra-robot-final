package driver_modes;

import java.util.Arrays;
import java.util.List;

import edu.wpi.first.wpilibj.GenericHID.Hand;
import state_derive.Button;
import state_derive.Context;

public class MODE_FullSpeedOneStick extends MODE_LinearDrive {
	static double m = 1.0;
	static double b = 0.0;

	//One stick to rule them all
	public MODE_FullSpeedOneStick() {
		super(m, b);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public List<Button> DefaultCombo() {
		return Arrays.asList(Button.B(), Button.Y());
	}

	@Override
	public void EnterMode(Context context) {
		// Do nothing
		
	}

	@Override
	public void ExitMode(Context context) {
		// Do nothing
		
	}
	
	@Override
	protected void drive(Context context, double m, double b) {
		double left = input_trans(context.controller.GetTriggerAxis(Hand.kLeft), m, b);
		double right = input_trans(context.controller.GetTriggerAxis(Hand.kRight), m , b);
		if(context.controller.GetBButton()) {
			left *= -1;
			right *= -1;
		}
		
		context.driveSystem.SetDrive(left, right);
	}

	@Override
	public String ModeName() {
		// TODO Auto-generated method stub
		return "Full Speed, One Stick";
	}
}
