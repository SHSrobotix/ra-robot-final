package driver_modes;

import java.util.List;

import edu.wpi.first.wpilibj.GenericHID.Hand;
import state_derive.Button;
import state_derive.Context;
import state_derive.dMode;
import state_machine.IState;

public abstract class MODE_LinearDrive  extends dMode<Context> {

	double m = 0.635;
	double b = 0.0;
	//TODO: Use button events
	public MODE_LinearDrive(double speed, double yintercept) {
		if (speed < 0.0) {
			throw new IllegalArgumentException();
		}
		
		m = speed;
		b = yintercept;
	}
	
	@Override
	public abstract List<Button> DefaultCombo();

	@Override
	public abstract void EnterMode(Context context);

	@Override
	public IState<Context> Update(Context context) {
		compressor(context);
		drive(context, m, b);
		return null;
	}

	@Override
	public abstract void ExitMode(Context context);

	protected void drive(Context context, double m, double b) {
		context.driveSystem.SetDrive(-input_trans(context.controller.GetY(Hand.kLeft), m, b), 
								-input_trans(context.controller.GetY(Hand.kRight), m, b));	
		if (context.controller.PressedX()) {
			context.driveSystem.SetDrive(-1, 1);
		}
	}
	
	protected double input_trans(double input, double m, double b) {
		return m * input + b;
	}
	
	protected void compressor(Context context) {
		if (context.controller.controller.getBumper(Hand.kRight) && context.controller.controller.getBumper(Hand.kLeft)) {
			context.kicker.Kick();
		} else {
			context.kicker.Retract();
		} 
		if (context.controller.PressedY()) {
			context.kicker.ToggleCompressor();
		}
	}
}
