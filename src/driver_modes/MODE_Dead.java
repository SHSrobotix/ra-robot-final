package driver_modes;

import java.util.Arrays;
import java.util.List;

import state_derive.Button;
import state_derive.Context;
import state_derive.dMode;
import state_machine.IState;

public class MODE_Dead extends dMode<Context> {

	@Override
	public List<Button> DefaultCombo() {
		// TODO Auto-generated method stub
		return Arrays.asList(Button.X(), Button.X());
	}

	@Override
	public void EnterMode(Context context) {
		// TODO Auto-generated method stub
		
		context.kicker.Retract();
	}

	@Override
	public IState<Context> Update(Context context) {
		return null;
	}

	@Override
	public void ExitMode(Context context) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String ModeName() {
		return "Dead Mode";
	}

}
