package subsystems;

import edu.wpi.first.wpilibj.MotorSafety;
import edu.wpi.first.wpilibj.MotorSafetyHelper;
import edu.wpi.first.wpilibj.Spark;
import edu.wpi.first.wpilibj.Victor;
import edu.wpi.first.wpilibj.command.Subsystem;

public class DriveSystem extends Subsystem implements MotorSafety {
	//TODO: Make these fields adjustable via SmartDashboard?
	static final int l1 = 1;
	static final int l2 = 2;
	static final int r1 = 0;
	static final int r2 = 3;
	
	static final double DEFAULT_SAFETY = 0.1;
	double maxOutput = 1.0;
	
	Victor left1;
	Victor left2;
	
	Victor right1;
	Victor right2;
	
	//TODO: Change to spark on final
	
	MotorSafetyHelper safetyHelper;
	
	public DriveSystem() {
		SetSafety();
		AllocateSpeedControllers();
	}
	
	public void free() {
	      left1.free();
	      left2.free();
	      right1.free();
	      right2.free();
	}
	
	/* Allocated motors MUST be freed MANUALLY
	 */
	
	void AllocateSpeedControllers() {
		//left1 = new Spark(l1);
		//left2 = new Spark(l2);

		//right1 = new Spark(r1);
		//right2 = new Spark(r2);
		
		left1 = new Victor(l1);
		left2 = new Victor(l2);

		right1 = new Victor(r1);
		right2 = new Victor(r2);
		
	}
	
	void SetSafety() {
		safetyHelper = new MotorSafetyHelper(this);
		safetyHelper.setExpiration(DEFAULT_SAFETY);
		safetyHelper.setSafetyEnabled(true);
	}
	
	public void SetDrive(double left, double right) {
		SetRight(right, true);
		SetLeft(left, true);
	}
	
	public void SetDrive(double left, double right, boolean squaredInputs) {
		SetRight(right, squaredInputs);
		SetLeft(left, squaredInputs);
	}
	
	public void SetRight(double value, boolean squaredInputs) {
	    double rightValue = limit(value);
	    
	    if (squaredInputs) {
	      if (rightValue >= 0.0) {
	    	  rightValue = rightValue * rightValue;
	      } else {
	    	  rightValue = -(rightValue * rightValue);
	      }
	    }
		
	    //flipped b/c opposing motors
		right1.set(-limit(rightValue) * maxOutput);
		right2.set(-limit(rightValue) * maxOutput);
		safetyHelper.feed();
	}
	
	public void SetLeft(double value, boolean squaredInputs) {
	    double leftValue = limit(value);
	    
	    if (squaredInputs) {
	      if (leftValue >= 0.0) {
	        leftValue = leftValue * leftValue;
	      } else {
	        leftValue = -(leftValue * leftValue);
	      }
	    }
		
		left1.set(limit(leftValue) * maxOutput);
		left2.set(limit(leftValue) * maxOutput);
		safetyHelper.feed();
	}
	
	double limit(double input) {
		if (input > 1.0) {
			return 1.0;
		} else if (input < -1.0) {
			return -1.0;
		}
		return input;
	}

	@Override
	public void setExpiration(double timeout) {
		safetyHelper.setExpiration(timeout);
	}

	@Override
	public double getExpiration() {
		return safetyHelper.getExpiration();
	}

	@Override
	public boolean isAlive() {
		return safetyHelper.isAlive();
	}

	@Override
	public void stopMotor() {
		SetDrive(0,0);
	}

	@Override
	public void setSafetyEnabled(boolean enabled) {
	    safetyHelper.setSafetyEnabled(enabled);
	}

	@Override
	public boolean isSafetyEnabled() {
		return safetyHelper.isSafetyEnabled();
	}

	@Override
	public String getDescription() {
		return "Robot Drive";
	}

	@Override
	protected void initDefaultCommand() {
		// TODO Auto-generated method stub
		
	}
}
