package subsystems;

import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.command.Subsystem;

public class Kicker extends Subsystem {
	//TODO: Make these fields adjustable via SmartDashboard?
	static final int CAN_ID = 0;
	static final int FORWARD_PCM_ID = 7;
	static final int REVERSE_PCM_ID = 6;
	
	Compressor compressor = new Compressor(CAN_ID);
	DoubleSolenoid solenoid = new DoubleSolenoid(FORWARD_PCM_ID, REVERSE_PCM_ID);
	
	public void SetCompressor(boolean state) {
		compressor.setClosedLoopControl(state);
	}
	
	public void ToggleCompressor() {
		compressor.setClosedLoopControl(!compressor.getClosedLoopControl());
	}
	
	public boolean GetCompressor() {
		return compressor.getClosedLoopControl();
	}
	
	public boolean GetSolenoid() {
		switch (solenoid.get()) {
		case kForward:
			return true;
		case kReverse:
		case kOff:
			return false;
		}
		return false;
	}
	
	public void Kick() {
		solenoid.set(Value.kForward);
	}
	
	public void Retract() {
		solenoid.set(Value.kReverse);
	}
	
	public void Off() {
		solenoid.set(Value.kOff);
	}

	@Override
	protected void initDefaultCommand() {
		// TODO Auto-generated method stub
		
	}
}
